﻿using System.Net;
using System.Net.Sockets;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BlueDotManager : MonoBehaviour {

    public Sprite blueDotSprite;
    public int maxNumSprites = 1;
    public float dotVelocity = 10.0f;

    private Camera mainCamera;
    private ArrayList dotPool;
    private ArrayList dotPoolFreeList;

    public float interpolationPeriod = 10.0f;
    private float elapsedTime = 0.0f;

    private TcpClient velocityServer = new TcpClient();
    private NetworkStream velocityServerStream;

    public int serverUpdatePeriod = 10;
    private int elapsedFrames = 0;
 
    // Use this for initialization
    void Start()
    {
        mainCamera = Camera.main;
        dotPool = new ArrayList(maxNumSprites);
        dotPoolFreeList = new ArrayList(maxNumSprites);

        try
        {
            IPAddress serverAddress = IPAddress.Parse("127.0.0.1");
            int serverPort = 4000;

            velocityServer.Connect(serverAddress, serverPort);
            velocityServerStream = velocityServer.GetStream();
        } catch (SocketException e) {
            Debug.Log("Unable to connect to the velocity server.");
            Debug.Log(e);
            velocityServer = null;
        }
    }

    // Update is called once per frame
    void Update()
    {
        if (velocityServer != null)
        {
            ++elapsedFrames;
            if (elapsedFrames >= serverUpdatePeriod)
            {
                if (velocityServerStream.DataAvailable)
                {
                    try
                    {
                        byte[] data = new byte[sizeof(float)];
                        velocityServerStream.Read(data, 0, sizeof(float));
                        float newVelocity = System.BitConverter.ToSingle(data, 0);
                        dotVelocity = newVelocity;
                    } catch {
                        Debug.Log("Lost connection to the velocity server.");
                        velocityServer.Close();
                        velocityServer = null;
                    }
                }
                elapsedFrames = 0;
            }
        }

        elapsedTime += Time.deltaTime;
        if (elapsedTime >= interpolationPeriod)
        {
            createDot();
            elapsedTime = 0.0f;
        }

        foreach (FallingDot dot in dotPool)
        {
            dot.update();
        }

        for (int i = 0; i < dotPool.Count; ++i)
        {
            FallingDot dot = (FallingDot)dotPool[i];
            if (!dot.isAlive())
            {
                dotPool.RemoveAt(i--);
                dotPoolFreeList.Add(dot);
            }
        }
    }

    private void createDot()
    {
        float viewportHalfHeight = mainCamera.orthographicSize;
        float viewportHalfWidth = (viewportHalfHeight * Screen.width) / Screen.height;
        float spriteHalfHeight = blueDotSprite.bounds.size.y;

        Vector3 cameraPosition = mainCamera.transform.position;
        Vector3 dotPosition = mainCamera.transform.position + mainCamera.transform.forward * 3.0f;
        float x = Random.Range(cameraPosition.x - viewportHalfWidth, cameraPosition.x + viewportHalfWidth);
        float y = dotPosition.y + viewportHalfHeight;

        if ((dotPool.Count + dotPoolFreeList.Count) < maxNumSprites)
        {
            FallingDot dot = new FallingDot(blueDotSprite, dotVelocity, x, y, -(viewportHalfHeight + spriteHalfHeight));
            dotPool.Add(dot);
        } else {
            if (dotPoolFreeList.Count > 0)
            {
                FallingDot dot = (FallingDot)dotPoolFreeList[0];
                dotPoolFreeList.RemoveAt(0);

                dot.setPosition(x, y);
                dot.setVelocity(dotVelocity);
                dot.setAlive();
                dotPool.Add(dot);
            }
        }
    }
}
