using UnityEngine;

public class FallingDot
{

    private GameObject gameObject;
    private SpriteRenderer spriteRenderer;
    private Vector3 velocity;
    private float deadline;

    private bool alive;

    public FallingDot(Sprite s, float v, float x, float y, float deadline)

    {
        gameObject = new GameObject("FallingDot");
        gameObject.transform.position = new Vector3(x, y, 0);

        gameObject.AddComponent<SpriteRenderer>();
        spriteRenderer = gameObject.GetComponent<SpriteRenderer>();

        this.deadline = deadline;
        spriteRenderer.sprite = s;

        setAlive();
        setVelocity(v);
    }

    public void update()
    {
        if (alive)
        {
            if (gameObject.transform.position.y < deadline)
            {
                alive = false;
            } else {
                gameObject.transform.position -= (velocity * Time.deltaTime);
            }
        }
    }

    public void setPosition(float x, float y)
    {
        gameObject.transform.position = new Vector3(x, y, 0);
    }

    public void setVelocity(float v)
    {
        velocity = new Vector3(0, v, 0);
    }

    public void setAlive()
    {
        alive = true;
    }

    public bool isAlive()
    {
        return alive;
    }
}
