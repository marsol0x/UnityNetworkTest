﻿using System;
using System.IO;
using System.Collections;
using System.Net;
using System.Net.Sockets;

namespace NetworkTestServer
{
    class Program
    {
        static void Main(string[] args)
        {
            TcpListener server = null;
            ArrayList clients = new ArrayList();

            TextReader consoleReader = Console.In;

            try
            {
                int port = 4000;
                IPAddress localhost = IPAddress.Parse("127.0.0.1");

                server = new TcpListener(localhost, port);
                server.Start();
                Console.Out.WriteLine("Server started...");

                while (true)
                {
                    // NOTE(marshel): We block here
                    Console.Out.Write("> ");
                    Console.Out.Flush();
                    String message = consoleReader.ReadLine();

                    if (server.Pending())
                    {
                        // NOTE(marshel): Pending socket connection
                        TcpClient newClient = server.AcceptTcpClient();
                        clients.Add(newClient);
                        Console.Out.WriteLine("\rConnected new client.");
                    }

                    for (int i = 0; i < clients.Count; ++i)
                    {
                        TcpClient client = (TcpClient)clients[i];
                        if (client.Connected)
                        {
                            NetworkStream clientStream = client.GetStream();

                            if (clientStream.CanRead)
                            {
                                // NOTE(helsperm): We're not reading from these client sockets
                            }

                            if (clientStream.CanWrite)
                            {
                                if (message != null)
                                {
                                    float object_velocity = float.Parse(message);
                                    byte[] data = System.BitConverter.GetBytes(object_velocity);

                                    try
                                    {
                                        clientStream.Write(data, 0, data.Length);
                                    } catch (IOException e) {
                                        Console.Out.WriteLine("\rClient connection closed.");
                                    }
                                }
                            }
                        } else {
                            // TODO(marshel): Client no longer connected
                            client.Close();
                            clients.RemoveAt(i--);
                        }
                    }
                }
            } catch (SocketException e) {
                Console.Out.WriteLine("SocketException: {0}", e);
            } finally {
                server.Stop();
            }
        }
    }
}
